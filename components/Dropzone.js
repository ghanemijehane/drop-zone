import { DownloadIcon } from '@heroicons/react/outline';
import React, { useCallback, useMemo, useState } from 'react'
import { useDropzone } from 'react-dropzone'
const baseStyle = {
    flex: 1,
    width: "350px",
    display: 'flex',
    justifyContent: 'center',
    margin: 'auto',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '20px',
    borderWidth: 2,
    borderRadius: 2,
    borderColor: '#eeeeee',
    borderStyle: 'dashed',
    backgroundColor: '#fafafa',
    color: '#bdbdbd',
    outline: 'none',
    transition: 'border .24s ease-in-out'
};
const focusedStyle = {
    borderColor: '#2196f3'
};

const acceptStyle = {
    borderColor: '#00e676'
};

const rejectStyle = {
    borderColor: '#ff1744'
};

function Dropzone() {
    const [selectedImages, setselectedImages] = useState([])
    const onDrop = useCallback(acceptedFiles => {
        setselectedImages(acceptedFiles.map(file =>
            Object.assign(file, {
                preview: URL.createObjectURL(file)
            })
        ))
    }, [])



    const { getRootProps, getInputProps, isFocused,
        isDragAccept,
        isDragReject, acceptedFiles } = useDropzone({
            onDrop,
            accept: "image/png,image/jpeg",
            multiple: false,

        });
    // const style = useMemo(() => ({
    //     ...baseStyle,
    //     ...(isFocused ? focusedStyle : {}),
    //     ...(isDragAccept ? acceptStyle : {}),
    //     ...(isDragReject ? rejectStyle : {}),
    // }), [
    //     isFocused,
    //     isDragAccept,
    //     isDragReject
    // ]);
    const selected_images = selectedImages?.map(file => (
        <div>
            <img src={file.preview} style={{ width: "200px" }} alt="" />
        </div>
    ))
    const files = acceptedFiles.map(file => (
        <li key={file.path}>
            {/* Try to convert file size from bytes to mo */}
            {file.path}- {file.size} bytes
        </li>
    ));
    return (
        <div className="p-4 w-full">

            <div
                className=" flex flex-col w-1/2 mx-auto justify-center rounded-md cursor-pointer h-80
          focus:outline-none" {...getRootProps({})}>

                <input {...getInputProps()} />
                <div className={
                    "flex flex-col items-center justify-center h-full space-y-3  border-2 border-dashed border-gray-400 rounded-xl " +
                (isDragReject === true ? "border-red-500":"") +
                (isDragAccept === true ? "border-green-500":"")
               }>
               
                    <DownloadIcon className="text-blue-400 h-8 w-8" />
                {
                    isDragReject ?
                        <p>Sorry, this app only supports images and pdf</p> :

                        <>
                            <p className="text-green-600">Drop the files here...</p>
                            {selected_images}
                            {files}
                        </>
                }


            </div>

        </div>

        </div >
    )
}

export default Dropzone
